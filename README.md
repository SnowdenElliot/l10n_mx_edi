# l10n_mx_edi

Localización mexicana EDI
Permita al usuario generar el documento EDI para la facturación mexicana.

Este módulo permite la creación de los documentos EDI y la comunicación con los proveedores de certificación mexicanos (PAC) para firmarlos/cancelarl